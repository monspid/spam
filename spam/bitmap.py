class Bitmap:
    def __init__(self, last_bit_index):
        self.__last_sequence_id = -1
        self.__support = 0
        self.__true_bits = list()
        self.__bitmap = self.__create_bitmap(last_bit_index)

    def __repr__(self):
        return str(self.__true_bits)

    def register_bit(self, sequence_id, itemset_id, sequences_size):
        position = sequences_size[sequence_id] + itemset_id
        self.set_bit(position)
        self.extend_support(sequence_id)

    def get_bit(self, position):
        return self.__bitmap[position]

    def set_bit(self, position):
        self.__bitmap[position] = True
        if position not in self.__true_bits:
            self.__true_bits.append(position)

    def get_true_bits(self, index=0):
        for bit in self.__true_bits:
            if bit >= index:
                yield bit

    @staticmethod
    def __create_bitmap(last_bit_index):
        return [False] * (last_bit_index + 1)

    def create_new_bitmap_s_step(self, bitmap_item, sequences_size, last_bit_index):
        new_bitmap = Bitmap(last_bit_index)

        for bit in self.get_true_bits():
            sequence_id = self.__bit_to_sequence_id(bit, sequences_size)
            last_bit_of_sequence_id = self.last_bit_of_sequence_id(sequence_id, sequences_size, last_bit_index)

            match = False
            for bit_ in bitmap_item.get_true_bits(index=bit + 1):
                if bit_ > last_bit_of_sequence_id:
                    break
                new_bitmap.set_bit(bit_)
                match = True

            if match:
                new_bitmap.extend_support(sequence_id)

        return new_bitmap

    def create_new_bitmap_i_step(self, bitmap_item, sequences_size, last_bit_index):
        new_bitmap = Bitmap(last_bit_index)

        for bit in self.get_true_bits():
            if bitmap_item.get_bit(bit):
                sequence_id = self.__bit_to_sequence_id(bit, sequences_size)
                new_bitmap.set_bit(bit)
                new_bitmap.extend_support(sequence_id)

        return new_bitmap

    def get_support(self):
        return self.__support

    def extend_support(self, sequence_id):
        if sequence_id != self.__last_sequence_id:
            self.__support += 1

        self.__last_sequence_id = sequence_id

    @staticmethod
    def __bit_to_sequence_id(bit, sequences_size):
        sequence_id = None
        for it, sequence_size in enumerate(sequences_size):
            if bit == sequence_size:
                sequence_id = it
                break
            elif bit < sequence_size:
                sequence_id = it - 1
                break
        sequence_id = sequence_id if sequence_id is not None else len(sequences_size) - 1
        return sequence_id

    @staticmethod
    def last_bit_of_sequence_id(sequence_id, sequences_size, last_bit_index):
        return sequences_size[sequence_id + 1] - 1 if sequence_id + 1 < len(sequences_size) else last_bit_index

    def get_sequences_ids(self, sequences_size):
        sequences_ids = set()
        for bit in self.get_true_bits():
            sequence_id = self.__bit_to_sequence_id(bit, sequences_size)
            sequences_ids.add(sequence_id)
        return sequences_ids
