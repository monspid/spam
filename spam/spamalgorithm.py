import copy
import os
from math import ceil

from sortedcontainers import SortedDict

from spam.bitmap import Bitmap


class SpamAlgorithm:
    def __init__(self):
        self.pattern_count = 0
        self.minimum_pattern_length = 0
        self.maximum_pattern_length = 1000
        self.last_bit_index = 0
        self.vertical_db = SortedDict()
        self.sequences_size = list()
        self.min_sup = 0
        self.align_sup = 0
        self.align_sid = 0
        self.out_path = "output"
        self.outfile = None

    def run(self, input_path, min_sup_relative=0):
        self.__set_outfile(input_path, min_sup_relative)
        self.last_bit_index = self.__fill_sequences_size(input_path)
        self.__set_min_sup(min_sup_relative)

        self.__fill_vertical_db(input_path)
        frequent_items = self.__get_frequent_items()
        self.__prune(frequent_items)

    def __set_outfile(self, filename, minsup_relative):
        self.outfile = open(os.path.join(self.out_path, str(minsup_relative) + os.path.basename(filename)), "w+")

    def __set_min_sup(self, min_sup_relative):
        self.min_sup = ceil(min_sup_relative * len(self.sequences_size)) or 1

    def __fill_sequences_size(self, input_):
        with open(input_) as f:
            bit_index = 0

            for line in f.readlines():
                line = line.strip()
                self.sequences_size.append(bit_index)

                for token in line.split(" "):
                    if token == "-1":
                        bit_index += 1

            self.align_sup = len(str(len(self.sequences_size)))
            self.align_sid = len(self.sequences_size) * 3

            return bit_index - 1

    def __fill_vertical_db(self, input_):
        with open(input_) as f:
            sequence_id = 0
            itemset_id = 0

            for line in f.readlines():
                line = line.strip()
                for token in line.split(" "):
                    if token == "-1":
                        itemset_id += 1
                    elif token == "-2":
                        sequence_id += 1
                        itemset_id = 0
                    else:
                        bitmap_item = self.vertical_db.setdefault(int(token), Bitmap(self.last_bit_index))
                        bitmap_item.register_bit(sequence_id, itemset_id, self.sequences_size)

    def __get_frequent_items(self):
        frequent_items = list()
        not_frequent_items = list()

        for key, value in self.vertical_db.items():
            if self.__is_frequent(value):
                if self.__should_save_one_item_pattern():
                    self.__save_pattern(key, value)

                frequent_items.append(key)
            else:
                not_frequent_items.append(key)

        for key in not_frequent_items:
            self.vertical_db.pop(key)

        return frequent_items

    def __should_save_one_item_pattern(self):
        return self.minimum_pattern_length <= 1 <= self.maximum_pattern_length

    def __prune(self, frequent_items):
        if self.__should_prune():
            for item, bitmap in self.vertical_db.items():
                self.__dfs_pruning([[item]], bitmap, frequent_items, frequent_items, item)

    def __should_prune(self):
        return self.maximum_pattern_length > 1

    def __is_frequent(self, bitmap):
        return bitmap.get_support() >= self.min_sup

    def __dfs_pruning(self, prefix, prefix_bitmap, s_n, i_n, grater_than_i, level=2):
        s_temp = SortedDict()

        for item in s_n:
            new_bitmap = prefix_bitmap.create_new_bitmap_s_step(self.vertical_db.get(item), self.sequences_size,
                                                                self.last_bit_index)

            if self.__is_frequent(new_bitmap):
                s_temp[item] = new_bitmap

        for item, new_bitmap in s_temp.items():
            prefix_s_step = copy.deepcopy(prefix)
            prefix_s_step.append([item])

            if self.__is_frequent(new_bitmap):
                if level >= self.minimum_pattern_length:
                    self.__save_pattern(prefix_s_step, new_bitmap)
                if self.maximum_pattern_length > level:
                    self.__dfs_pruning(prefix_s_step, new_bitmap, s_temp, s_temp, item, level + 1)

        i_temp = SortedDict()

        for item in i_n:
            if item > grater_than_i:
                new_bitmap = prefix_bitmap.create_new_bitmap_i_step(self.vertical_db.get(item), self.sequences_size,
                                                                    self.last_bit_index)

                if self.__is_frequent(new_bitmap):
                    i_temp[item] = new_bitmap

        for item, new_bitmap in i_temp.items():
            prefix_i_step = copy.deepcopy(prefix)
            prefix_i_step[-1].append(item)

            if level >= self.minimum_pattern_length:
                self.__save_pattern(prefix_i_step, new_bitmap)

            if self.maximum_pattern_length > level:
                self.__dfs_pruning(prefix_i_step, new_bitmap, s_temp, i_temp, item, level + 1)

    def __save_pattern(self, prefix, bitmap):
        self.pattern_count += 1

        if not isinstance(prefix, list):
            prefix = [[prefix]]

        pattern = "SUP: {0:<{align_sup}} SID: {1:<{align_sid}} SEQ: ".format(
            bitmap.get_support(),
            str(bitmap.get_sequences_ids(self.sequences_size)),
            align_sup=self.align_sup,
            align_sid=self.align_sid
        )
        pattern += "; ".join(str(itemset) for itemset in prefix)

        self.outfile.write(pattern + "\n")
        print(pattern)
