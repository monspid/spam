# SPAM Algorithm
SPAM Algorithm - Sequential PAttern Mining using A Bitmap Representation


## Basic Info
Application has been tested with Python 3.7 (Windows), but should also work in Python 2.7.
Before usage install requirements from requirements.txt.

## How to run
python main.py

Then enter required data (dataset path and minsup)

## How to build a data set
Data sets are build with following rules:
 - each sequence must start from new line;
 - each sequence must be finished with "-2" character;
 - each itemset must be followed with "-1" character;
 - each item in itemset must be a number;
 - each item in itemset must be followed by a space;
 - items in itemset should be ordered;
 óó