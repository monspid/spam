if __name__ == "__main__":
    with open("atlanta.txt") as f:
        with open("atlanta_opt.txt", "w+") as fo:
            for line in f.readlines():
                line = line.strip()
                seqs = line.split()
                seqs_list = list()
                for seq in seqs:
                    seqs_list.append(int(seq))

                seqs_list.sort()

                seq_str = ""
                for seq in seqs_list:
                    seq_str += str(seq) + " -1 "

                seq_str += "-2\n"

                fo.write(seq_str)




