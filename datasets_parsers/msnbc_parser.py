if __name__ == "__main__":
    count_lines = 0
    with open("msnbc.txt") as f:
        with open("msnbc_opt.txt", "w+") as fo:
            for line in f.readlines():
                line = line.strip()
                seqs = line.split()

                seqs_str = " -1 ".join(seqs) + " -1 -2\n"
                fo.write(seqs_str)

                count_lines += 1

                if count_lines > 20000:
                    break
