import os
from datetime import datetime

from spam.spamalgorithm import SpamAlgorithm


def get_input():
    global path, minsup
    path = input("Enter dataset path: ")
    if not os.path.exists(path):
        print("File does not exists")
        exit(-1)
    try:
        minsup = input("Enter minsup [float]: ")
        minsup = float(minsup)
    except Exception as e:
        print(e)
        exit(-1)


def run_algorithm():
    print("================================ SPAM RESULTS ================================")
    spam_algorithm = SpamAlgorithm()
    start_time = datetime.now()
    spam_algorithm.run(path, minsup)
    end_time = datetime.now()
    print("==============================================================================", end="\n\n")
    print("============================== SPAM STATISTICS ===============================")
    print("MINSUP:", spam_algorithm.min_sup)
    print("PATTERN COUNT:", spam_algorithm.pattern_count)
    print("TIME:", end_time - start_time)
    print("==============================================================================")


if __name__ == '__main__':
    get_input()
    run_algorithm()
